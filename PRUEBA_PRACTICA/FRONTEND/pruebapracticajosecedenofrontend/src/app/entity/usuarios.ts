import {Citas} from "./citas";

export class Usuarios {
  idusuario?: number;
  firstName?: string;
  lastName?: string;
  email?: string;
  age?: number;
  image?: string;
  citas?: Citas[];
}
