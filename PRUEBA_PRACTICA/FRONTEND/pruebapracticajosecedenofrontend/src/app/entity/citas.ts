import {Usuarios} from "./usuarios";

export class Citas {

  idcita?: number;
  numero?: number;
  fecha?: Date;
  hora?: string;
  especialidad?: string;
  observacion?: string;
  usuarios?: Usuarios;
}
