import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MaterialModule} from "../material/material.module";
import {RouterModule, Routes} from "@angular/router";
import { InicioComponent } from './components/inicio/inicio.component';
import {HttpClientModule} from "@angular/common/http";
import {ReactiveFormsModule} from "@angular/forms";
import { UsuariosComponent } from './components/usuarios/usuarios.component';
import { CitaComponent } from './components/cita/cita.component';
import { ListarcitasComponent } from './components/listarcitas/listarcitas.component';

const routes: Routes = [
  {path: 'usuarios', component: UsuariosComponent},
  {path: 'citas', component: CitaComponent},
  {path: 'listarcitas', component: ListarcitasComponent},
]

@NgModule({
  declarations: [
    AppComponent,
    InicioComponent,
    UsuariosComponent,
    CitaComponent,
    ListarcitasComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    ReactiveFormsModule,
    HttpClientModule,
    [RouterModule.forRoot(routes, {useHash: false})],
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
