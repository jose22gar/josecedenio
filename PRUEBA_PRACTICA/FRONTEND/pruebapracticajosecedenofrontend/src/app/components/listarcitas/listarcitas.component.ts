import {Component, OnInit} from '@angular/core';
import {Usuarios} from "../../entity/usuarios";
import {UsuarioService} from "../../service/usuario.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {CitasService} from "../../service/citas.service";
import {Citas} from "../../entity/citas";
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-listarcitas',
  templateUrl: './listarcitas.component.html',
  styleUrls: ['./listarcitas.component.css']
})
export class ListarcitasComponent implements OnInit{

  displayedColumns: string[] = ['idcita', 'numero', 'fecha', 'hora', 'especialidad', 'observacion', 'usuarios','editar','eliminar'];


  dataSource: Citas[] = [];

  constructor(private usuarioService: UsuarioService,
              private _snackBar: MatSnackBar,
              private citasService:CitasService) {
  }

  formGroup = new FormGroup({
    idcita: new FormControl<number|undefined>(undefined,Validators.required),
    fecha: new FormControl<Date|undefined>(undefined,Validators.required),
    numero: new FormControl<number|undefined>(undefined,Validators.required),
    hora: new FormControl<string|undefined>('',Validators.required),
    especialidad: new FormControl<string|undefined>('',Validators.required),
    observacion: new FormControl<string|undefined>('',Validators.required),
    usuarios: new FormControl<Usuarios|undefined>(undefined),
  });


  actualiazarUsuario(cita: Citas) {
    this.formGroup.setValue({
      especialidad: cita.especialidad,
      fecha: cita.fecha,
      hora: cita.hora,
      idcita: cita.idcita,
      observacion: cita.observacion,
      usuarios: cita.usuarios,
      numero: cita.numero
    })
  }

  guardarUsuario() {
    // @ts-ignore
    this.citasService.update(this.formGroup.getRawValue()).subscribe(value => {
      this._snackBar.open("Usuario creado con exito", "", {
        duration: 2000
      });
      this.listarCitas();
    }, error => {
      this._snackBar.open(error.error.message, "", {
        duration: 2000
      })
    })
  }

  eliminarCitas(id: number) {
    this.citasService.deleteById(id).subscribe(value => {
      this._snackBar.open("Creado se elimino correctamente", "", {
        duration: 2000
      });
      this.listarCitas();
    }, error => {
      this._snackBar.open(error.error.message, "", {
        duration: 2000
      });
    })
  }

  listarCitas(){
    this.citasService.findAll().subscribe(value => {
      this.dataSource = value;
    })
  }
  ngOnInit(): void {
    this.listarCitas();
  }

}
