import {Component, OnInit} from '@angular/core';
import {UsuarioService} from "../../service/usuario.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Usuarios} from "../../entity/usuarios";
import {CitasService} from "../../service/citas.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-cita',
  templateUrl: './cita.component.html',
  styleUrls: ['./cita.component.css']
})
export class CitaComponent implements OnInit{

  date = new Date();
  usuario:Usuarios = new Usuarios();

  constructor(private usuarioService: UsuarioService,
              private _snackBar: MatSnackBar,
              private citasService:CitasService,
              private router:Router) {
  }
  formGroupEmail = new FormGroup({
    email: new FormControl('',[Validators.required,Validators.email]),
  });

  formGroup = new FormGroup({
    fecha: new FormControl<Date|undefined>(undefined,Validators.required),
    hora: new FormControl<string|undefined>('',Validators.required),
    especialidad: new FormControl<string|undefined>('',Validators.required),
    observacion: new FormControl<string|undefined>('',Validators.required),
    usuarios: new FormControl<Usuarios|undefined>(this.usuario),
  });

  ngOnInit(): void {
    this.date=new Date();
  }

  buscarUsuario(){
    // @ts-ignore
    this.usuarioService.findByEmail(this.formGroupEmail.getRawValue().email).subscribe(value => {
      this._snackBar.open("Usuario encontrado correctamente", "", {
        duration: 2000
      });
      this.formGroup.setValue({
        especialidad: "", fecha: null, hora: "", observacion: "", usuarios: value

      })
      this.usuario=value;
    },error => {
      this._snackBar.open(error.error.message, "", {
        duration: 2000
      });
    })
  }

  guardarCita(){
    // @ts-ignore
    this.citasService.save(this.formGroup.getRawValue()).subscribe(value => {
      this._snackBar.open("Cita creada correctamente", "", {
        duration: 2000
      });
      this.router.navigate(['/listarcitas']);
    },error => {
      this._snackBar.open(error.error.message, "", {
        duration: 2000
      });
    })
  }

}
