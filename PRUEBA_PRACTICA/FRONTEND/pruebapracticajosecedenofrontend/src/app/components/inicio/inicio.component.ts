import { Component } from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent {

  logero = false;


  formGroup = new FormGroup({
    userName: new FormControl<string|undefined>(undefined),
    password: new FormControl<string|undefined>(''),
  });


  inicarSesion(){
    if(this.formGroup.getRawValue().userName=="admin" && this.formGroup.getRawValue().password=="admin"){
      this.logero=true;
    }
  }

}
