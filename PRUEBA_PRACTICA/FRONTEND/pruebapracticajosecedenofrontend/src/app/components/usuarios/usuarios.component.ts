import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {UsuarioService} from "../../service/usuario.service";
import {Usuarios} from "../../entity/usuarios";
import {MatSnackBar} from "@angular/material/snack-bar";


@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})
export class UsuariosComponent implements OnInit {

  displayedColumns: string[] = ['id', 'firstName', 'lastName', 'age', 'email', 'image', 'eliminar', 'editar'];


  dataSource: Usuarios[] = [];

  constructor(private fb: FormBuilder,
              private usuarioService: UsuarioService,
              private _snackBar: MatSnackBar) {
  }

  ngOnInit(): void {
    this.listarUsuarios();
  }


  listarUsuarios() {
    this.usuarioService.findAll().subscribe(value => {
      this.dataSource = value;
    })
  }

  formGroup = new FormGroup({
    idusuario: new FormControl<number | undefined>(undefined),
    firstName: new FormControl<string | undefined>(''),
    lastName: new FormControl<string | undefined>(''),
    age: new FormControl<number | undefined>(undefined),
    email: new FormControl<string | undefined>(''),
    image: new FormControl<string | undefined>(''),
  });

  eliminarUsuario(id: number) {
    this.usuarioService.deleteById(id).subscribe(value => {
      this._snackBar.open("Usuario se elimino correctamente", "", {
        duration: 2000
      });
      this.listarUsuarios();
    }, error => {
      this._snackBar.open(error.error.message, "", {
        duration: 2000
      });
    })
  }

  guardarUsuario() {
    if (this.formGroup.getRawValue().idusuario == -1) {
      // @ts-ignore
      this.usuarioService.save(this.formGroup.getRawValue()).subscribe(value => {
        this._snackBar.open("Usuario creado con exito", "", {
          duration: 2000
        });
        this.limmparcampos();
        this.listarUsuarios();
      }, error => {
        this._snackBar.open(error.error.message, "", {
          duration: 2000
        })
      })
    } else {
      // @ts-ignore
      this.usuarioService.update(this.formGroup.getRawValue()).subscribe(value => {
        this._snackBar.open("Usuario actualizado con exito", "", {
          duration: 2000
        });
        this.limmparcampos();
        this.listarUsuarios();
      }, error => {
        this._snackBar.open(error.error.message, "", {
          duration: 2000
        })
      })
    }
  }

  actualiazarUsuario(usuario: Usuarios) {
    this.formGroup.setValue({
      age: usuario.age,
      email: usuario.email,
      firstName: usuario.firstName,
      idusuario: usuario.idusuario,
      image: usuario.image,
      lastName: usuario.lastName

    })
  }

  subirImagen(file: FileList) {
    console.log(file[0])
    const reader = new FileReader();
    reader.readAsDataURL(file[0]);
    reader.onload = () => {
      let imagenEnBase64 = reader.result;
      this.formGroup.setValue({
        age: this.formGroup.getRawValue().age,
        email: this.formGroup.getRawValue().email,
        firstName: this.formGroup.getRawValue().firstName,
        idusuario: this.formGroup.getRawValue().idusuario,
        image: imagenEnBase64+"",
        lastName: this.formGroup.getRawValue().lastName,
      })
    }

  }

  limmparcampos() {
    this.formGroup.setValue({
      age: 0,
      email: '',
      firstName: '',
      idusuario: -1,
      image: '',
      lastName: ''
    })
  }

  agregarUsuaios() {
    this.usuarioService.endPoint().subscribe(value => {
      // @ts-ignore
      this.usuarioService.saveAll(value.users).subscribe(value1 => {
        this._snackBar.open("Usuarios agregados", "", {
          duration: 2000
        });
        this.listarUsuarios();
      }, error => {
        this._snackBar.open(error.error.message, "", {
          duration: 2000
        })
      })
    }, error => {
      this._snackBar.open(error.error.message, "", {
        duration: 2000
      })
    })
  }


}

