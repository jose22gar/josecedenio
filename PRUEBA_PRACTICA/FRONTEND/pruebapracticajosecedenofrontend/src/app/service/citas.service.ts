import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {map, Observable} from "rxjs";
import {Prueba} from "../entity/prueba";
import {environment} from "../../environments/environment";
import {Citas} from "../entity/citas";

@Injectable({
  providedIn: 'root'
})
export class CitasService {

  constructor(private http: HttpClient) {
  }

  findAll(): Observable<Citas[]> {
    return this.http.get(environment.URL_APP + "/citas/findAll").pipe(map(Response => Response as Citas[]));
  }

  findById(id: number): Observable<Citas> {
    return this.http.get(environment.URL_APP + "/citas/findById/" + id);
  }

  save(response: Citas): Observable<Citas> {
    return this.http.post(environment.URL_APP + "/citas/save", response);
  }

  update(response: Citas): Observable<Citas> {
    return this.http.put(environment.URL_APP + "/citas/update", response);
  }

  deleteById(id: number): Observable<Prueba> {
    return this.http.delete(environment.URL_APP + "/citas/deleteById/" + id);
  }

  signUp(prueba: Prueba): Observable<Prueba> {
    return this.http.post(environment.URL_APP + "/prueba/signup", prueba);
  }
}
