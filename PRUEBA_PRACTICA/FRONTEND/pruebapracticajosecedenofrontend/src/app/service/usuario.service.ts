import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {map, Observable} from "rxjs";
import {Prueba} from "../entity/prueba";
import {environment} from "../../environments/environment";
import {Usuarios} from "../entity/usuarios";
import {FormControl, ɵFormGroupRawValue, ɵTypedOrUntyped} from "@angular/forms";
import {EndPoint} from "../entity/end-point";

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  constructor(private http: HttpClient) {
  }

  findAll(): Observable<Usuarios[]> {
    return this.http.get(environment.URL_APP + "/usuarios/findAll").pipe(map(Response => Response as Usuarios[]));
  }

  findById(id: number): Observable<Usuarios> {
    return this.http.get(environment.URL_APP + "/usuarios/findById/" + id);
  }

  findByEmail(email: string): Observable<Usuarios> {
    return this.http.get(environment.URL_APP + "/usuarios/findByEmail/" + email);
  }

  save(response: Usuarios): Observable<Usuarios> {
    return this.http.post(environment.URL_APP + "/usuarios/save", response);
  }


  saveAll(response: Usuarios[]): Observable<Usuarios> {
    return this.http.post(environment.URL_APP + "/usuarios/saveAll", response);
  }

  update(response: Usuarios): Observable<Usuarios> {
    return this.http.put(environment.URL_APP + "/usuarios/update", response);
  }

  deleteById(id: number): Observable<Usuarios> {
    return this.http.delete(environment.URL_APP + "/usuarios/deleteById/" + id);
  }

  signUp(response: Usuarios): Observable<Usuarios> {
    return this.http.post(environment.URL_APP + "/prueba/signup", response);
  }


  endPoint(): Observable<EndPoint> {
    return this.http.get( "https://dummyjson.com/users");
  }
}
