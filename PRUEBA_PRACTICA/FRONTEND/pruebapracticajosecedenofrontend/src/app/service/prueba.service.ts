import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Prueba} from "../entity/prueba";
import {map, Observable} from "rxjs";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class PruebaService {

  constructor(private http: HttpClient) {
  }

  findAll(): Observable<Prueba[]> {
    return this.http.get(environment.URL_APP + "/prueba/findAll").pipe(map(Response => Response as Prueba[]));
  }

  findById(id: number): Observable<Prueba> {
    return this.http.get(environment.URL_APP + "/prueba/findById/" + id);
  }

  save(persona: Prueba): Observable<Prueba> {
    return this.http.post(environment.URL_APP + "/prueba/save", persona);
  }

  update(persona: Prueba): Observable<Prueba> {
    return this.http.put(environment.URL_APP + "/prueba/update", persona);
  }

  deleteById(id: number): Observable<Prueba> {
    return this.http.delete(environment.URL_APP + "/prueba/deleteById/" + id);
  }

  signUp(prueba: Prueba): Observable<Prueba> {
    return this.http.post(environment.URL_APP + "/prueba/signup", prueba);
  }
}
