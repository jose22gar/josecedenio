# Pruebapracticajosecedenofront

Este proyecto fue realizado con [Angular CLI](https://github.com/angular/angular-cli) version 15.1.3.

## Dependencias

El proyecto de FrontEnd una las siguentes dependincias.

1. Comando para depencencias node.
2. Angular Material, propio de Angular CLI para mejorar
   el estilo visual de los componentes.

## Comandos para instalar dependencias

1. `npm install`

Nota: Al instalar Angular Material, seleccione en themes, "costum", y en animation "Si".


## Servidor de desarrollo

Ejecute `ng serve` para un servidor de desarrollo. Navegue a `http://localhost:4200/`. La aplicación se recargará automáticamente 
si cambia alguno de los archivos de origen.


## Dockerizar proyecto
Nota: Ejecute el siguente comando: `ng analytics prompt`, aparecera 
una confirmación con dos opciones de Si(`y`) y No(`N`), le daremos en No(`N`).
1. En la carpeta environments se encuentra la url donde el proyecto consumirá el BackEnd `http://localhost:8080/api`  
2. Ejecutamos el siguente comando para crear la imagen: `docker build -t "prueba-practica-front-jose-cedeno" .`
3. Una vez finalizado el proceso anterior, ejecutamos el siguente comando para iniciar el 
container: `docker run --name pruebapracticafrontjosecedeno -p 80:80 prueba-practica-front-jose-cedeno:latest`.

Nota: No cambiar el puerto: `-p 80:80` puesto que el BackEnd escuchara ha este puerto. 


