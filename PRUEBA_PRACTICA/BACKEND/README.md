# pruebapracticajosecedeno

## Datos del proyecto

- Proyecto creado en Spring Boot 2.7.6 (https://start.spring.io/). 
- Uso de java jdk 19.

## Depenciancias utilizadas 

- PosgresDriver
- Spring Web
- Spring Boot DevToos
- Spring Data JPA
- Lombok
- Swagger 3.0.0

## Pasos para iniciar proyecto

1. Crear una base en posgreSQL, propiedades en el archivo aplication.properties. Nombre de la base es: 
`pruebapracticajosecedenodb`, la url sera `host.docker.internal:32769` y el usuario y clave son:
`postgres` y `2001`

2. Limpiar dependencias Mavel con el siguiente comando:
`mvn clean`.
3. Instalar dependencias Mavel con el siguiente comando: `mvn install`.
4. Arrancar el proyecto con el siguente comando: `mvn spring-boot:run`; puerto por defecto `8080`


## Pasos para dockerizar el proyecto

2. Limpiar el proyecto usando el siguente comando: `maven clean`.
3. Compilar el proyecto usando el siguente comando: `maven install `.
4. Una vez compilado, se creara un archivo .jar, en el carpeta target.
5. Revisar que todo los parametros del nombre de aplicativo este correcto en el archivo Dockerfile.
6. Por último ejecutamos el siguente comando: `docker-compose up`, nos creara una imagen del progreSql 15. Adémas
se creará un container de progreSql con los siguentes datos: `POSTGRES_USER=postgres, POSTGRES_PASSWORD=2001,
   POSTGRES_DB=pruebapracticajosecedenodb, ports: '32769:5432'`. Adémas de la imagen del proyecto y su container.

Nota: El proyecto escuchará y aceptará consumos de las siguentes rutas: `"http://localhost", "http://localhost:4200"`.