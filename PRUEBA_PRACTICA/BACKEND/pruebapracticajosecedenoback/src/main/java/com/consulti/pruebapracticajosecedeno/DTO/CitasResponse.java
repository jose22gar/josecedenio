package com.consulti.pruebapracticajosecedeno.DTO;

import com.consulti.pruebapracticajosecedeno.Entity.Usuarios;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;


@Getter
@Setter
public class CitasResponse implements Serializable {

    private Long idcita;

    private int numero;

    private Date fecha;

    private String hora;

    private String especialidad;

    private String observacion;

    private UsuarioResponse usuarios;

    public CitasResponse() {
    }

}
