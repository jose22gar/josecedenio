package com.consulti.pruebapracticajosecedeno;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PruebapracticajosecedenoApplication {

    public static void main(String[] args) {
        SpringApplication.run(PruebapracticajosecedenoApplication.class, args);
    }

}
