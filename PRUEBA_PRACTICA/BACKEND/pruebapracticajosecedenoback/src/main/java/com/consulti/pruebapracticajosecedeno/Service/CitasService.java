package com.consulti.pruebapracticajosecedeno.Service;


import com.consulti.pruebapracticajosecedeno.DTO.CitasResponse;
import com.consulti.pruebapracticajosecedeno.DTO.UsuarioResponse;
import com.consulti.pruebapracticajosecedeno.Entity.Citas;
import com.consulti.pruebapracticajosecedeno.Entity.Usuarios;
import com.consulti.pruebapracticajosecedeno.Repository.CitasRepository;
import com.consulti.pruebapracticajosecedeno.Repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

@Service
public class CitasService {

    @Autowired
    private CitasRepository citasRepository;
    @Autowired
    private UsuarioRepository usuarioRepository;

    /**
     * Funcion que crea un citas.
     *
     * @param: Citas request
     * @return: Citas
     * @author: José Cedeño <jose22ced@gmail.com>
     */
    @Transactional
    public Citas save(Citas request) {
        int numero = 0;
        Citas citas = request;
        while (true){
            numero = new Random().nextInt(1000-1)+1;
            Optional<Citas> citas1 = citasRepository.findByNumero(numero);
            if(!citas1.isPresent()){
                citas.setNumero(numero);
                break;
            }
        }
        citas.setUsuarios(usuarioRepository.findById(request.getUsuarios().getIdusuario()).get());
        return citasRepository.save(citas);
    }

    @Transactional
    public Citas update(Citas request) {
        Citas citas = request;
        citas.setUsuarios(usuarioRepository.findById(request.getUsuarios().getIdusuario()).get());
        return citasRepository.save(citas);
    }


    /**
     * Funcion que lista todas los citas.
     *
     * @param: ninguna
     * @return: List<CitasResponse>
     * @author: José Cedeño <jose22ced@gmail.com>
     */
    @Transactional
    public List<CitasResponse> findAll() {
       List<Citas> citas = citasRepository.findAll();

        return citas.stream().map(citas1 -> {
            CitasResponse citasResponse = new CitasResponse();
            citasResponse.setIdcita(citas1.getIdcita());
            citasResponse.setFecha(citas1.getFecha());
            citasResponse.setHora(citas1.getHora());
            citasResponse.setNumero(citas1.getNumero());
            citasResponse.setEspecialidad(citas1.getEspecialidad());
            citasResponse.setObservacion(citas1.getObservacion());
            citasResponse.setUsuarios(new UsuarioResponse(
                    citas1.getUsuarios().getIdusuario(),
                    citas1.getUsuarios().getFirstName(),
                    citas1.getUsuarios().getLastName(),
                    citas1.getUsuarios().getAge(),
                    citas1.getUsuarios().getEmail(),
                    citas1.getUsuarios().getImage()));
            return citasResponse;
        }).collect(Collectors.toList());
    }

    /**
     * Funcion que busca a una cita.
     *
     * @param: Long id
     * @return: Citas
     * @author: José Cedeño <jose22ced@gmail.com>
     */
    @Transactional
    public Citas findById(Long id) {
        return citasRepository.findById(id).get();
    }

    /**
     * Funcion que elimina a una cita.
     *
     * @param: Long id
     * @return: ninguno
     * @author: José Cedeño <jose22ced@gmail.com>
     */
    @Transactional
    public void deleteById(Long id) {
        citasRepository.deleteById(id);
    }
}
