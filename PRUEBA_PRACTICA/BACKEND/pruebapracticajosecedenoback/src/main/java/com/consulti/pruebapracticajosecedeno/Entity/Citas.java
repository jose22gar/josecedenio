package com.consulti.pruebapracticajosecedeno.Entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.NotFound;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Entity
@Table(name = "citas")
@Getter
@Setter
public class Citas implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "idcita", nullable = false)
    private Long idcita;

    @Column(name = "numero", nullable = false)
    private int numero;

    @Column(name = "fecha", nullable = false)
    private Date fecha;

    @Column(name = "hora", nullable = false)
    private String hora;

    @Column(name = "especialidad", nullable = false, length = 50)
    private String especialidad;


    @Column(name = "observacion", nullable = false, length = 150)
    private String observacion;


    @JsonBackReference
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "idusuario", referencedColumnName = "idusuario")
    private Usuarios usuarios;


}
