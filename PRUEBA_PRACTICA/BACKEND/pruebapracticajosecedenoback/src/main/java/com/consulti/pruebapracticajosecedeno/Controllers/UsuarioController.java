package com.consulti.pruebapracticajosecedeno.Controllers;


import com.consulti.pruebapracticajosecedeno.Entity.Usuarios;
import com.consulti.pruebapracticajosecedeno.Service.UsuariosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = {"http://localhost", "http://localhost:4200"})
@RestController
@RequestMapping("api/usuarios")
public class UsuarioController {

    @Autowired
    private UsuariosService usuariosService;


    @GetMapping("/findAll")
    private ResponseEntity<List<Usuarios>> findAll() {
        return new ResponseEntity<List<Usuarios>>(usuariosService.findAll(), HttpStatus.OK);
    }

    @PostMapping("/save")
    public ResponseEntity<Usuarios> save(@RequestBody Usuarios request) {
        return new ResponseEntity<Usuarios>(usuariosService.save(request), HttpStatus.CREATED);
    }

    @PostMapping("/saveAll")
    public ResponseEntity<List<Usuarios>> saveAll(@RequestBody List<Usuarios> request) {
        return new ResponseEntity<List<Usuarios>>(usuariosService.saveAll(request), HttpStatus.CREATED);
    }

    @PutMapping("/update")
    public ResponseEntity<Usuarios> update(@RequestBody Usuarios request) {
        return new ResponseEntity<Usuarios>(usuariosService.update(request), HttpStatus.CREATED);
    }

    @GetMapping("/findById/{id}")
    public ResponseEntity<Usuarios> findById(@PathVariable Long id) {
        return new ResponseEntity<Usuarios>(usuariosService.findById(id), HttpStatus.ACCEPTED);
    }

    @GetMapping("/findByEmail/{email}")
    public ResponseEntity<Usuarios> findById(@PathVariable String email) {
        return new ResponseEntity<Usuarios>(usuariosService.findByEmail(email), HttpStatus.ACCEPTED);
    }

    @DeleteMapping("/deleteById/{id}")
    public ResponseEntity<Boolean> deleteById(@PathVariable Long id) {
        usuariosService.deleteById(id);
        return new ResponseEntity<Boolean>(true, HttpStatus.ACCEPTED);
    }
}
