package com.consulti.pruebapracticajosecedeno.Controllers;


import com.consulti.pruebapracticajosecedeno.DTO.CitasResponse;
import com.consulti.pruebapracticajosecedeno.Entity.Citas;
import com.consulti.pruebapracticajosecedeno.Service.CitasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = {"http://localhost", "http://localhost:4200"})
@RestController
@RequestMapping("api/citas")
public class CitasController {

    @Autowired
    private CitasService citasService;


    @GetMapping("/findAll")
    private ResponseEntity<List<CitasResponse>> findAll() {
        return new ResponseEntity<List<CitasResponse>>(citasService.findAll(), HttpStatus.OK);
    }

    @PostMapping("/save")
    public ResponseEntity<Citas> save(@RequestBody Citas request) {
        return new ResponseEntity<Citas>(citasService.save(request), HttpStatus.CREATED);
    }

    @PutMapping("/update")
    public ResponseEntity<Citas> update(@RequestBody Citas request) {
        return new ResponseEntity<Citas>(citasService.update(request), HttpStatus.CREATED);
    }

    @GetMapping("/findById/{id}")
    public ResponseEntity<Citas> findById(@PathVariable Long id) {
        return new ResponseEntity<Citas>(citasService.findById(id), HttpStatus.ACCEPTED);
    }

    @DeleteMapping("/deleteById/{id}")
    public ResponseEntity<Boolean> deleteById(@PathVariable Long id) {
        citasService.deleteById(id);
        return new ResponseEntity<Boolean>(true, HttpStatus.ACCEPTED);
    }
}
