package com.consulti.pruebapracticajosecedeno.Service;

import com.consulti.pruebapracticajosecedeno.Entity.Usuarios;
import com.consulti.pruebapracticajosecedeno.Exceptions.BadRequestException;
import com.consulti.pruebapracticajosecedeno.Repository.UsuarioRepository;
import com.consulti.pruebapracticajosecedeno.Validators.UsuarioValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class UsuariosService {

    @Autowired
    private UsuarioRepository usuarioRepository;


    private UsuarioValidator usuarioValidator = new UsuarioValidator();

    /**
     * Funcion que crea un usuario.
     *
     * @param: Usuarios
     * @return: Usuarios
     * @author: José Cedeño <jose22ced@gmail.com>
     */
    @Transactional
    public Usuarios save(Usuarios request) {
        String mesaje = usuarioValidator.correoUsuarios2(usuarioRepository.findByEmail(request.getEmail()));
        if(mesaje==null){
            return usuarioRepository.save(request);
        }else {
            throw new BadRequestException(mesaje);
        }
    }

    /**
     * Funcion que actualizar un usuario.
     *
     * @param: Usuarios
     * @return: Usuarios
     * @author: José Cedeño <jose22ced@gmail.com>
     */
    @Transactional
    public Usuarios update(Usuarios request) {
        return usuarioRepository.save(request);
    }


    /**
     * Funcion que crear varios usuario.
     *
     * @param: List<Usuarios>
     * @return: List<Usuarios>
     * @author: José Cedeño <jose22ced@gmail.com>
     */
    @Transactional
    public List<Usuarios> saveAll(List<Usuarios> request) {
        return usuarioRepository.saveAll(request);
    }



    /**
     * Funcion que lista todos los usuario.
     *
     * @param: ninguna
     * @return: List<Usuarios>
     * @author: José Cedeño <jose22ced@gmail.com>
     */
    @Transactional
    public List<Usuarios> findAll() {
        return usuarioRepository.findAll();
    }

    /**
     * Funcion que busca a una usuario.
     *
     * @param: Long id
     * @return: Usuarios
     * @author: José Cedeño <jose22ced@gmail.com>
     */
    @Transactional
    public Usuarios findById(Long id) {
        return usuarioRepository.findById(id).get();
    }


    /**
     * Funcion que busca a una usuario por correo.
     *
     * @param: String s
     * @return: Usuarios
     * @author: José Cedeño <jose22ced@gmail.com>
     */
    @Transactional
    public Usuarios findByEmail(String s) {
        String mesaje = usuarioValidator.correoUsuarios(usuarioRepository.findByEmail(s));
        if(mesaje==null){
            return usuarioRepository.findByEmail(s).get();
        }else {
           throw new BadRequestException(mesaje);
        }
    }

    /**
     * Funcion que elimina a una usuario.
     *
     * @param: Long id
     * @return: ninguno
     * @author: José Cedeño <jose22ced@gmail.com>
     */
    @Transactional
    public void deleteById(Long id) {
        usuarioRepository.deleteById(id);
    }
}
