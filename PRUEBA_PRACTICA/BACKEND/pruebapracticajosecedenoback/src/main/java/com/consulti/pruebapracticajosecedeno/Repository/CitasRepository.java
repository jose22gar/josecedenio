package com.consulti.pruebapracticajosecedeno.Repository;

import com.consulti.pruebapracticajosecedeno.Entity.Citas;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CitasRepository extends JpaRepository<Citas, Long> {
    Optional<Citas> findByNumero(int i);
}
