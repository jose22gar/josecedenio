package com.consulti.pruebapracticajosecedeno.Entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;


@Entity
@Table(name = "usuarios")
@Getter
@Setter
public class Usuarios implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "idusuario", nullable = false)
    private Long idusuario;

    @Column(name = "firstName", nullable = false , length = 50)
    private String firstName;

    @Column(name = "lastName", nullable = false, length = 50)
    private String lastName;

    @Column(name = "age", nullable = false, length = 2)
    private int age;

    @Column(name = "email", nullable = false,length = 50, unique = true)
    private String email;

    @Column(name = "image", length = 10485760)
    private String image;

    @JsonManagedReference
    @OneToMany(targetEntity = Citas.class,mappedBy = "usuarios")
    private List<Citas> citas;

}
