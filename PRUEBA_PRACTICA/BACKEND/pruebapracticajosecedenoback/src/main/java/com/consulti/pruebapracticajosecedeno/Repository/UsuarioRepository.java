package com.consulti.pruebapracticajosecedeno.Repository;

import com.consulti.pruebapracticajosecedeno.Entity.Usuarios;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UsuarioRepository extends JpaRepository<Usuarios,Long> {

    Optional<Usuarios> findByEmail(String s);

}
