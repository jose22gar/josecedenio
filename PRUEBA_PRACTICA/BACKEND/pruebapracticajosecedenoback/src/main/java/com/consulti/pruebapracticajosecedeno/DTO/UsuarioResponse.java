package com.consulti.pruebapracticajosecedeno.DTO;


import com.consulti.pruebapracticajosecedeno.Entity.Citas;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class UsuarioResponse implements Serializable {

    private Long idusuario;


    private String firstName;


    private String lastName;


    private int age;


    private String email;


    private String image;

    public UsuarioResponse(Long idusuario, String firstName, String lastName, int age, String email, String image) {
        this.idusuario = idusuario;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.email = email;
        this.image = image;
    }
}
