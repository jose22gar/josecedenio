/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package generarcartes;

import java.util.Random;

/**
 *
 * @author jose2
 */
public class GenerarCartas {

    //Creamos una instancia estatica llamando la clase Ramdom
    private static Random random = new Random();
    //Creamos una instancia del anunciado Palo, el cual almacenara el palo anterior.
    private static Palo paloanterior = null;

    public static void main(String[] args) {
        //Creamos un buble hasta 10 que impimira los 10 valores;
        for (int i = 0; i < 10; i++) {
            //Unando lo valores del buble queremos saber si el numero es par.
            boolean residuo = i % 2 == 0;
            //Imprimimos en pantalla los las catartes, pero primero llamamos 
            //al metodo  generar cartas con el parametro de residuo;
            System.out.println(generarCartas(residuo));
        }
    }

    // Creamos un enunciado llamado palo donde tendremos todos los timpos decara;
    enum Palo {
        CORAZON_ROJO, CORAZON_NEGRO, DIAMANTE, TREBOL
    }

    /*Creamos un metodo estatico de tipo String para obtner la carte generada
    @parametro: boolean residuo
    @return: String
     */
    private static String generarCartas(boolean residuo) {
        Palo palo = generarPalo();
        String carta;
        if (residuo) {
            //Genera un numero candom de 9 al 2;
            int numero = random.nextInt(9) + 2;
            carta = numero + " de " + palo;
        } else {
            //Genera un numero candom de 9 al 2;
            String[] figuras = {"J", "Q", "K", "AS"};
            int figuraramdom = random.nextInt(figuras.length);
            carta = figuras[figuraramdom] + " de " + palo;
        }
        paloanterior = palo;
        return carta;
    }

    /*Creamos un metodo estatico de tipo Palo del enunciado para genrar el Palo
    @parametro: ninguno
    @return: Palo
     */
    private static Palo generarPalo() {
        Palo palo;
        //Creamos un bucle que lo rompera cuando el palo genreado sea igual al palo enterior, 
        //el do while sale del buclu cuando este sea falso;
        do {
            //Optendra el un numero ramdom con la cantidad de datos del enum palo.
            int paloramdom = random.nextInt(Palo.values().length);
            //Con el numero random optenido escojera un dato de palo.
            palo = Palo.values()[paloramdom];
        } while (palo == paloanterior);
        return palo;
    }

}
