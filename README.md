# Jose Cedeño

#PASOS PARA INICIAR EL PROYECTO
#PRÁCTICO:
1. BACKEND
a. El proyecto está realizado con 2.7.6. Con la versión de JDK 19.
b. Las dependencias que conforman el proyecto son: PosgresDriver,
Spring Web, Spring Boot DevToos, Spring Data JPA, Lombok, Swagger
3.0.0.
c. La base de datos que usa el proyecto es PosgreSQL. Con los
siguientes datos:
- Nombre de la base datos: pruebapracticajosecedenodb
- username: postgres
- password: 2001
- URL:jdbc:postgresql://host.docker.internal:32769/prue
bapracticajosecedenodb
d. El host que usa el proyecto para conectarse a la base de datos es
host.docker.interna y el puesto 32769:5432
e. El proyecto se levantará en el puerto 8080:8080.
f. El proyecto escuchara y aceptara los corps de las siguientes rutas:
“http://localhost", "http://localhost:4200”
g. Para compilar el proyecto usaremos el siguiente comando: “mvc clean”
para limpiar y el “mvn install” para compilar y crear el .jar. En la Carpeta
target.
h. Para correr el proyecto ejecutamos el siguiente comando:
“mvn spring-boot:run”
#Dockerizar BackEnd.
i. Para dockerizar el proyecto y la base de datos por consola nos
dirigimos en la carpeta raíz del proyecto ejecutamos el siguiente
comando: “docker-compose up”
j. El comando anterior creará una imagen del postgreSQL 15, creará el
contenedor de datos para que el proyecto se conecte con la base.
Además de la imagen del proyecto y el contendor en el puerto
8080:8080
k. Swagger “http://localhost:8080/swagger-ui/#/”
Nombre: José Cedeño

2. FRONTEND
a. El proyecto se realizó con Angular CLI 15.1.3
b. Las dependencias que conforman el proyecto son: Angular Materia.
c. Para instalar las dependencias ejecutamos el siguiente comando: npm
install.
d. Para correr el proyecto ejecutamos el siguiente comando: ng serve, el
proyecto se levantará en el puerto: 4200.
#Dockerizar FrontEnd.
e. Para dockerizar primero ejecutamos el siguiente comando: ng
analytics prompt, aparecerá una confirmación con dos opciones de
Si(y) y No(N), le daremos en No(N).
f. Luego en la carpeta raíz ejecutamos el siguiente comando: docker
‘build -t "prueba-practica-front-jose-cedeno" .’ . Nos creará la
imagen del proyecto.
g. Una vez terminado el proceso ejecutamos el siguiente comando:
‘docker run --name pruebapracticafrontjosecedeno -p 80:80
prueba-practica-front-jose-cedeno:latest’ . Nos creará el container
del puerto 80:80.
h. No cambiar el puesto 80:80 puesto que el BackEnd escuchará a este
puerto